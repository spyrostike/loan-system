package main

import "loan-system/internal/container"

func main() {
	container := &container.Container{}

	container.Run()
}
