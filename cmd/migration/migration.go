package main

import "loan-system/internal/container"

func main() {
	container.NewContainer().Migrate()
}
