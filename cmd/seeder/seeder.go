package main

import (
	"loan-system/internal/config"
	"loan-system/internal/entity"
	"loan-system/internal/infrastructure/database"
	"loan-system/internal/utils"
)

func main() {
	env, _ := config.NewConfiguration()
	db, _ := database.NewDB(env)

	// datetime := time.Now()

	db.Connection.Create(&entity.Test{
		NameEN: "Test",
		NameTH: "ทดสอบ",
	})

	hash, _ := utils.HashPassword("123456")

	db.Connection.Create(&entity.Admin{
		Username:  "administrator",
		Password:  hash,
		FirstName: "super",
		LastName:  "admin",
		Email:     "administrator@mail.com",
		MobileNo:  "0812345678",
		ImageProfiles: []entity.ImageProfile{
			{
				Path:    "reoefb",
				Name:    "3sdsdsdg23",
				Type:    "34",
				SubType: "44",
				Size:    4343,
				Tag:     "xx",
				Status:  entity.ACTIVE,
			},
		},
		Role:   entity.SUPER_ADMIN,
		Status: entity.ACTIVE,
	})
}
