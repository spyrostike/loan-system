package container

import (
	"context"
	"loan-system/internal/config"
	"loan-system/internal/controller"
	"loan-system/internal/gin/route"
	"loan-system/internal/graph/resolver"
	"loan-system/internal/infrastructure/database"
	"loan-system/internal/infrastructure/http"
	"loan-system/internal/infrastructure/middleware"
	"loan-system/internal/repository"
	"loan-system/internal/service"
	"loan-system/internal/utils"

	"go.uber.org/fx"
)

type Container struct{}

// NewContainer ..
func NewContainer() *Container {
	return new(Container)
}

func (c *Container) configure() []fx.Option {
	return []fx.Option{
		config.Module,
		controller.Module,
		database.Module,
		http.Module,
		middleware.Module,
		repository.Module,
		resolver.Module,
		route.Module,
		service.Module,
		utils.Module,
	}
}

func runApplication(
	lifecycle fx.Lifecycle,
	server *http.GinHTTPServer,
) {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(ctx context.Context) error {
				go server.Start(ctx)
				return nil
			},
			OnStop: func(ctx context.Context) error {
				return server.Stop(ctx)
			},
		},
	)
}

func migrateDatabase(db *database.DB) {
	db.Migrate()
}

func (c *Container) Run() {
	options := append(c.configure(), fx.Invoke(runApplication))
	fx.New(
		options...,
	).Run()
}

func (c *Container) Migrate() {
	options := append(c.configure(), fx.Invoke(migrateDatabase))

	fx.New(
		options...,
	).Start(context.Background())

}
