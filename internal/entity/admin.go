package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// Admin Model
type Admin struct {
	ID            string         `gorm:"type:uuid;primary_key;" json:"id"`
	Username      string         `gorm:"type:varchar(100);unique_index;not null;" json:"username"`
	Password      string         `gorm:"type:varchar(120);not null;" json:"password"`
	FirstName     string         `gorm:"type:varchar(100);not null;" json:"firstName"`
	LastName      string         `gorm:"type:varchar(100);not null;" json:"lastName"`
	Email         string         `gorm:"type:varchar(50);not null;" json:"email"`
	MobileNo      string         `gorm:"type:varchar(10);" json:"mobileNo"`
	ImageProfiles []ImageProfile `gorm:"foreignKey:RefID;references:ID"`
	Role          AdminRole      `sql:"type:enum('SUPER_ADMIN', 'GENERAL_ADMIN');" json:"role"`
	Status        Status         `sql:"type:enum('ACTIVE', 'INACTIVE');" json:"status"`
	CreatedAt     *time.Time     `gorm:"column:created_at" json:"createdAt"`
	CreatedBy     *string        `gorm:"type:uuid" json:"createdBy"`
	UpdatedAt     *time.Time     `gorm:"column:updated_at" json:"updatedAt"`
	UpdatedBy     *string        `gorm:"type:uuid" json:"updatedBy"`
	DeletedAt     *time.Time     `gorm:"column:deleted_at" json:"deletedAt"`
	DeletedBy     *string        `gorm:"type:uuid" json:"deletedBy"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (a *Admin) BeforeCreate(tx *gorm.DB) error {
	a.ID = uuid.NewString()
	return nil
}

// TableName Rename
func (Admin) TableName() string {
	return "admins"
}
