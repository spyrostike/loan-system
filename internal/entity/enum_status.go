package entity

import (
	"database/sql/driver"
	"errors"
	"fmt"
)

//Status type
type Status string

const (
	ACTIVE   Status = "ACTIVE"
	INACTIVE Status = "INACTIVE"
)

//Value validate enum when set to database
func (s Status) Value() (driver.Value, error) {
	switch s {
	case ACTIVE, INACTIVE: //valid case
		return string(s), nil
	}
	return nil, errors.New("invalid status value") //else is invalid
}

//Scan validate enum on read from data base
func (s *Status) Scan(value interface{}) error {
	var status Status
	if value == nil {
		*s = ""
		return nil
	}
	st, ok := value.(string) // if we declare db type as ENUM gorm will scan value as []uint8
	if !ok {
		return errors.New("invalid data for status")
	}
	status = Status(string(st)) //convert type from string to ProductType

	switch status {
	case ACTIVE, INACTIVE: //valid case
		*s = status
		return nil
	}
	return fmt.Errorf("invalid status value :%s", st) //else is invalid
}
