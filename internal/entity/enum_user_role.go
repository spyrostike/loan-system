package entity

import (
	"database/sql/driver"
	"errors"
	"fmt"
)

//UserRole type
type UserRole string

const (
	USER UserRole = "USER"
)

//Value validate enum when set to database
func (u UserRole) Value() (driver.Value, error) {
	switch u {
	case USER: //valid case
		return string(u), nil
	}
	return nil, errors.New("invalid admin role value") //else is invalid
}

//Scan validate enum on read from data base
func (u *UserRole) Scan(value interface{}) error {
	var userRole UserRole
	if value == nil {
		*u = ""
		return nil
	}
	st, ok := value.(string) // if we declare db type as ENUM gorm will scan value as []uint8
	if !ok {
		return errors.New("invalid data for admin role")
	}
	userRole = UserRole(string(st)) //convert type from string to ProductType

	switch userRole {
	case USER: //valid case
		*u = userRole
		return nil
	}
	return fmt.Errorf("invalid admin role value :%s", st) //else is invalid
}
