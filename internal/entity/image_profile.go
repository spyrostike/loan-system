package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// ImageProfile Model
type ImageProfile struct {
	ID        string     `gorm:"type:uuid;primary_key;" json:"id"`
	Path      string     `gorm:"type:varchar(20);" json:"path"`
	Name      string     `gorm:"type:varchar(30);" json:"name"`
	Type      string     `gorm:"type:varchar(20);" json:"type"`
	SubType   string     `gorm:"type:varchar(20);" json:"subType"`
	Size      int        `json:"size"`
	Tag       string     `gorm:"type:varchar(20);" json:"tag"`
	RefID     string     `gorm:"type:uuid;" json:"refID"`
	Status    Status     `sql:"type:enum('ACTIVE', 'INACTIVE');" json:"status"`
	CreatedAt *time.Time `gorm:"column:created_at" json:"createdAt"`
	CreatedBy *string    `gorm:"type:uuid" json:"createdBy"`
	UpdatedAt *time.Time `gorm:"column:updated_at" json:"updatedAt"`
	UpdatedBy *string    `gorm:"type:uuid" json:"updatedBy"`
	DeletedAt *time.Time `gorm:"column:deleted_at" json:"deletedAt"`
	DeletedBy *string    `gorm:"type:uuid" json:"deletedBy"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (i *ImageProfile) BeforeCreate(tx *gorm.DB) error {
	i.ID = uuid.NewString()
	return nil
}

// TableName Rename
func (ImageProfile) TableName() string {
	return "image_profiles"
}
