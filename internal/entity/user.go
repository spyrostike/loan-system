package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// User Model
type User struct {
	ID            string         `gorm:"type:uuid;primary_key;" json:"id"`
	Username      string         `gorm:"type:varchar(100);unique_index;not null;" json:"username"`
	Password      string         `gorm:"type:varchar(120);not null;" json:"password"`
	FirstName     string         `gorm:"type:varchar(100);not null;" json:"firstName"`
	LastName      string         `gorm:"type:varchar(100);not null;" json:"lastName"`
	Email         string         `gorm:"type:varchar(50);not null;" json:"email"`
	MobileNo      string         `gorm:"type:varchar(10);not null" json:"mobileNo"`
	SalaryRate    string         `gorm:"type:float;" json:"salaryRate"`
	ImageProfiles []ImageProfile `gorm:"foreignKey:RefID;references:ID"`
	PositionID    string         `gorm:"type:uuid;" json:"positionId"`
	Position      Position       `gorm:"foreignKey:PositionID"`
	DepartmentID  string         `gorm:"type:uuid;" json:"departmentID"`
	Department    Department     `gorm:"foreignKey:DepartmentID"`
	BranchID      string         `gorm:"type:uuid;" json:"branchID"`
	Branch        Position       `gorm:"foreignKey:BranchID"`
	Gender        Gender         `sql:"type:enum('MALE', 'FEMALE');" json:"gender"`
	Adress        string         `sql:"type:text;" json:"adress"`
	ProviceID     int            `gorm:"type:int;" json:"proviceID"`
	Province      Province       `gorm:"foreignKey:ProviceID"`
	DistrictID    int            `gorm:"type:int;" json:"districtID"`
	District      District       `gorm:"foreignKey:DistrictID"`
	SubDistrictID int            `gorm:"type:int;" json:"subDistrictID"`
	SubDistrict   SubDistrict    `gorm:"foreignKey:SubDistrictID"`
	Status        Status         `sql:"type:enum('ACTIVE', 'INACTIVE');" json:"status"`
	CreatedAt     *time.Time     `gorm:"column:created_at" json:"createdAt"`
	CreatedBy     *string        `gorm:"type:uuid" json:"createdBy"`
	UpdatedAt     *time.Time     `gorm:"column:updated_at" json:"updatedAt"`
	UpdatedBy     *string        `gorm:"type:uuid" json:"updatedBy"`
	DeletedAt     *time.Time     `gorm:"column:deleted_at" json:"deletedAt"`
	DeletedBy     *string        `gorm:"type:uuid" json:"deletedBy"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (u *User) BeforeCreate(tx *gorm.DB) error {
	u.ID = uuid.NewString()
	return nil
}

// TableName Rename
func (User) TableName() string {
	return "users"
}
