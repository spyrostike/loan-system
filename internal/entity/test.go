package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// Test Model
type Test struct {
	ID        string     `gorm:"type:uuid;primary_key;" json:"id"`
	NameEN    string     `gorm:"type:varchar(50)" json:"nameEN"`
	NameTH    string     `gorm:"type:varchar(50)" json:"nameTH"`
	CreatedAt *time.Time `gorm:"column:created_at" json:"createdAt"`
	UpdatedAt *time.Time `gorm:"column:updated_at" json:"updatedAt"`
	DeletedAt *time.Time `gorm:"column:deleted_at" json:"deletedAt"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (t *Test) BeforeCreate(tx *gorm.DB) error {
	t.ID = uuid.NewString()
	return nil
}

// TableName Rename
func (Test) TableName() string {
	return "test"
}
