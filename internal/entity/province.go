package entity

// Test Model
type Province struct {
	ID          int    `gorm:"column:id;primary_key;" json:"id"`
	Code        string `gorm:"type:varchar(2)" json:"code"`
	NameEN      string `gorm:"type:varchar(150)" json:"nameEN"`
	NameTH      string `gorm:"type:varchar(150)" json:"nameTH"`
	GeographyID int    `json:"geographyId"`
}

// TableName Rename
func (Province) TableName() string {
	return "provinces"
}
