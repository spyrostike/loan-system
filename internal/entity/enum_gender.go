package entity

import (
	"database/sql/driver"
	"errors"
	"fmt"
)

//Gender type
type Gender string

const (
	MALE   Gender = "MALE"
	FEMAIL Gender = "FEMAIL"
)

//Value validate enum when set to database
func (s Gender) Value() (driver.Value, error) {
	switch s {
	case MALE, FEMAIL: //valid case
		return string(s), nil
	}
	return nil, errors.New("invalid status value") //else is invalid
}

//Scan validate enum on read from data base
func (g *Gender) Scan(value interface{}) error {
	var gender Gender
	if value == nil {
		*g = ""
		return nil
	}
	st, ok := value.(string) // if we declare db type as ENUM gorm will scan value as []uint8
	if !ok {
		return errors.New("invalid data for gender")
	}
	gender = Gender(string(st)) //convert type from string to ProductType

	switch gender {
	case MALE, FEMAIL: //valid case
		*g = gender
		return nil
	}
	return fmt.Errorf("invalid gender value :%s", st) //else is invalid
}
