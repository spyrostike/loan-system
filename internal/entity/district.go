package entity

// Test Model
type District struct {
	ID         int    `gorm:"column:id;primary_key;" json:"id"`
	Code       string `gorm:"type:varchar(4)" json:"code"`
	NameEN     string `gorm:"type:varchar(150)" json:"nameEN"`
	NameTH     string `gorm:"type:varchar(150)" json:"NameTH"`
	ProvinceID int    `json:"provinceID"`
}

// TableName Rename
func (District) TableName() string {
	return "districts"
}
