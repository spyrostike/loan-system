package entity

// Test Model
type Geography struct {
	ID   int    `gorm:"column:id;primary_key;" json:"id"`
	Name string `gorm:"type:varchar(255)" json:"name"`
}

// TabRen
func (Geography) TableName() string {
	return "geographies"
}
