package entity

// Test Model
type SubDistrict struct {
	ID         int    `gorm:"column:id;primary_key;" json:"id"`
	ZipCode    int    `json:"zipCode"`
	NameEN     string `gorm:"type:varchar(150)" json:"nameEN"`
	NameTH     string `gorm:"type:varchar(150)" json:"NameTH"`
	DistrictID int    `json:"district_id"`
}

// TableName Rename
func (SubDistrict) TableName() string {
	return "sub_districts"
}
