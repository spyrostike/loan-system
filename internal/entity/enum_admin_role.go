package entity

import (
	"database/sql/driver"
	"errors"
	"fmt"
)

//AdminRole type
type AdminRole string

const (
	SUPER_ADMIN AdminRole = "SUPER_ADMIN"
	ADMIN       AdminRole = "ADMIN"
)

//Value validate enum when set to database
func (a AdminRole) Value() (driver.Value, error) {
	switch a {
	case SUPER_ADMIN, ADMIN: //valid case
		return string(a), nil
	}
	return nil, errors.New("invalid admin role value") //else is invalid
}

//Scan validate enum on read from data base
func (a *AdminRole) Scan(value interface{}) error {
	var adminRole AdminRole
	if value == nil {
		*a = ""
		return nil
	}
	st, ok := value.(string) // if we declare db type as ENUM gorm will scan value as []uint8
	if !ok {
		return errors.New("invalid data for admin role")
	}
	adminRole = AdminRole(string(st)) //convert type from string to ProductType

	switch adminRole {
	case SUPER_ADMIN, ADMIN: //valid case
		*a = adminRole
		return nil
	}
	return fmt.Errorf("invalid admin role value :%s", st) //else is invalid
}
