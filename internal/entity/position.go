package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// Position Model
type Position struct {
	ID        string     `gorm:"type:uuid;primary_key;" json:"id"`
	Code      string     `gorm:"type:varchar(10)" json:"code"`
	NameEN    string     `gorm:"type:varchar(150)" json:"nameEN"`
	NameTH    string     `gorm:"type:varchar(150)" json:"nameTH"`
	Status    Status     `sql:"type:enum('ACTIVE', 'INACTIVE')" json:"status"`
	CreatedAt *time.Time `gorm:"column:created_at" json:"createdAt"`
	CreatedBy *string    `gorm:"type:uuid" json:"createdBy"`
	UpdatedAt *time.Time `gorm:"column:updated_at" json:"updatedAt"`
	UpdatedBy *string    `gorm:"type:uuid" json:"updatedBy"`
	DeletedAt *time.Time `gorm:"column:deleted_at" json:"deletedAt"`
	DeletedBy *string    `gorm:"type:uuid" json:"deletedBy"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (p *Position) BeforeCreate(tx *gorm.DB) error {
	p.ID = uuid.NewString()
	return nil
}

// TableName Rename
func (Position) TableName() string {
	return "positions"
}
