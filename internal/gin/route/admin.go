package route

import (
	"loan-system/internal/controller"

	"github.com/gin-gonic/gin"
)

// Admin ...
type AdminRoute struct {
	adminCtrl *controller.AdminController
}

// NewAdminRoute ...
func NewAdminRoute(AdminCtrl *controller.AdminController) *AdminRoute {
	return &AdminRoute{
		adminCtrl: AdminCtrl,
	}
}

// setupRouter
func (a *AdminRoute) RoutesGrouping(r *gin.Engine) {
	v1 := r.Group("/admin")
	{
		v1.POST("/authen", a.adminCtrl.Authen)
	}
}
