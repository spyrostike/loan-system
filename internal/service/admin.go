package service

import (
	"fmt"
	"loan-system/internal/entity"
	"loan-system/internal/repository"
	"loan-system/internal/utils"
)

type AdminService struct {
	admin  *repository.Admin
	crypto *utils.Crypto
}

func NewAdminService(admin *repository.Admin, crypto *utils.Crypto) *AdminService {
	return &AdminService{
		admin:  admin,
		crypto: crypto,
	}
}

// Authen admin
func (service *AdminService) Authen(admin *entity.Admin) (string, error) {
	res, err := service.admin.Authen(&entity.Admin{Username: admin.Username})

	if err != nil {
		return "", fmt.Errorf("ไม่พบผู้ใช้งาน %s", admin.Username)
	}

	if res.Status != entity.ACTIVE {
		return "", fmt.Errorf("ผู้ใช้ยังไม่ถูกเปิดใช้งาน")
	}

	if !utils.CheckPasswordHash(admin.Password, res.Password) {
		return "", fmt.Errorf("รหัสผ่านไม่ถูกต้อง")
	}

	accessToken, err := service.crypto.CreateToken(res.ID, string(res.Role))

	if err != nil {
		return "", err
	}
	return accessToken, nil
}

// Add admin
func (service *AdminService) Add(req *entity.Admin) (*entity.Admin, error) {
	if len(req.Username) < 6 {
		return nil, fmt.Errorf("username should be more than 6")
	}

	if len(req.Password) < 6 {
		return nil, fmt.Errorf("password should be more than 6")
	}

	res, _ := service.admin.GetByCondition(&entity.Admin{Username: req.Username, Status: entity.ACTIVE})

	if res != nil {
		return nil, fmt.Errorf("username [%s] has been registered", req.Username)
	}

	hash, _ := utils.HashPassword(req.Password)
	req.Password = hash
	req.Status = entity.ACTIVE
	req.Role = entity.ADMIN
	res, err := service.admin.Add(req)

	if err != nil {
		return nil, err
	}
	return res, nil
}

// Get admin
func (service *AdminService) Get(admin *entity.Admin) (*entity.Admin, error) {
	res, err := service.admin.Get(&entity.Admin{ID: admin.ID})
	// fmt.Println("xxxxxxxxx", res.ID)
	if err != nil {
		return nil, err
	}

	return res, nil
}
