package service

import (
	"loan-system/internal/entity"
	"loan-system/internal/repository"
)

type TestService struct {
	test *repository.Test
}

func NewTestService(test *repository.Test) *TestService {
	return &TestService{
		test: test,
	}
}

func (service *TestService) Add(test *entity.Test) (string, error) {
	err := service.test.Add(test)

	if err != nil {
		return "", err
	}

	return test.ID, nil
}

func (service *TestService) Update(test *entity.Test) (string, error) {
	err := service.test.Update(test)

	if err != nil {
		return "", err
	}

	return test.ID, nil
}

func (service *TestService) Get(test *entity.Test) (*entity.Test, error) {
	res := &entity.Test{}
	err := service.test.Get(&entity.Test{ID: test.ID}, &res)

	if err != nil {
		return nil, err
	}

	return res, nil
}
