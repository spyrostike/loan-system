package service

import (
	"fmt"
	"loan-system/internal/entity"
	"loan-system/internal/repository"
	"loan-system/internal/utils"
)

type BranchService struct {
	branch *repository.Branch
	crypto *utils.Crypto
}

func NewBranchService(branch *repository.Branch, crypto *utils.Crypto) *BranchService {
	return &BranchService{
		branch: branch,
		crypto: crypto,
	}
}

// Add admin
func (service *BranchService) Add(req *entity.Branch) (*entity.Branch, error) {
	if len(req.Code) != 10 {
		return nil, fmt.Errorf("brach code should be 10 characters")
	}

	if len(req.NameEN) <= 0 {
		return nil, fmt.Errorf("branch name_en is required.")
	}

	if len(req.NameTH) <= 0 {
		return nil, fmt.Errorf("branch name_th is required.")
	}

	res, _ := service.branch.Get(&entity.Branch{Code: req.Code, Status: entity.ACTIVE})

	if res != nil {
		return nil, fmt.Errorf("branch [%s] is exist", req.Code)
	}

	req.Status = entity.ACTIVE
	res, err := service.branch.Add(req)

	if err != nil {
		return nil, err
	}
	return res, nil
}
