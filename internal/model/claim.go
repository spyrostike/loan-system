package model

import "github.com/dgrijalva/jwt-go"

type AuthTokenClaim struct {
	UserID string `json:"user_id"`
	Role   string `json:"role"`
	*jwt.StandardClaims
}
