package resolver

import (
	"loan-system/internal/config"
	"loan-system/internal/controller"
	"loan-system/internal/infrastructure/middleware"
	"loan-system/internal/utils"

	"go.uber.org/fx"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

var Module = fx.Provide(NewResolver)

type Resolver struct {
	config *config.Configuration
	ctrl   ctrl
	crypto *utils.Crypto
	header *middleware.Header
}

type ctrl struct {
	fx.In
	AdminCtrl    *controller.AdminController
	BranchCtrl   *controller.BranchController
	PingPongCtrl *controller.PingPongController
}

func NewResolver(
	config *config.Configuration,
	ctrl ctrl,
	crypto *utils.Crypto,
	header *middleware.Header,
) *Resolver {
	resolver := Resolver{
		config: config,
		ctrl:   ctrl,
		crypto: crypto,
		header: header,
	}
	return &resolver
}
