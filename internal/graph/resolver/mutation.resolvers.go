package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"loan-system/internal/entity"
	"loan-system/internal/graph/generated"
	"loan-system/internal/graph/model"
	"loan-system/internal/utils"
)

func (r *mutationResolver) PingPong(ctx context.Context, input model.PingPongInput) (*model.PingPongResponse, error) {
	res, err := r.ctrl.PingPongCtrl.PingPong(input)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (r *mutationResolver) TestAdd(ctx context.Context, input model.TestInput) (*model.PingPongResponse, error) {
	res, err := r.ctrl.PingPongCtrl.Add(input)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (r *mutationResolver) TestUpdate(ctx context.Context, input model.TestInput) (*model.PingPongResponse, error) {
	res, err := r.ctrl.PingPongCtrl.Update(input)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (r *mutationResolver) CreateAdmin(ctx context.Context, input *model.AdminInput) (*model.AdminResponse, error) {
	var resp *entity.Admin
	utils.ConvertObj(&input, &resp)
	resp, err := r.ctrl.AdminCtrl.Add(resp)

	if err != nil {
		return nil, err
	}

	var convert *model.AdminResponse
	utils.ConvertObj(&resp, &convert)

	return convert, nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input *model.UserInput) (string, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) CreateBranch(ctx context.Context, input *model.BranchInput) (string, error) {
	var resp *entity.Branch
	utils.ConvertObj(&input, &resp)
	resp, err := r.ctrl.BranchCtrl.Add(resp)

	if err != nil {
		return "", err
	}

	var convert *model.AdminResponse
	utils.ConvertObj(&resp, &convert)

	return resp.ID, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
