package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"loan-system/internal/graph/generated"
	"loan-system/internal/graph/model"
)

func (r *queryResolver) PingPong(ctx context.Context, input model.PingPongInput) (*model.PingPongResponse, error) {
	res, err := r.ctrl.PingPongCtrl.PingPong(input)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (r *queryResolver) TestGet(ctx context.Context, input model.TestInput) (*model.PingPongResponse, error) {
	resp, err := r.ctrl.PingPongCtrl.Get(input)

	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (r *queryResolver) GetAdmin(ctx context.Context, id string) (*model.AdminResponse, error) {
	resp, err := r.ctrl.AdminCtrl.Get(id)

	if err != nil {
		return nil, err
	}

	return resp, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
