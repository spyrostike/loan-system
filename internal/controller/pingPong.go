package controller

import (
	"fmt"
	"loan-system/internal/entity"
	"loan-system/internal/graph/model"
	"loan-system/internal/service"
	"time"
)

// PinPongController ...
type PingPongController struct {
	testService *service.TestService
}

// PinPongController ...
func NewPingPongController(testService *service.TestService) *PingPongController {
	return &PingPongController{
		testService: testService,
	}
}

// PingPong ...
func (p *PingPongController) PingPong(input model.PingPongInput) (*model.PingPongResponse, error) {
	return &model.PingPongResponse{
		Msg:  input.Msg + " ctrl",
		Ball: input.Ball + 100,
	}, nil
}

// PingPong ...
func (p *PingPongController) Add(input model.TestInput) (*model.PingPongResponse, error) {
	test := &entity.Test{
		NameEN: *input.NameEn,
		NameTH: *input.NameTh,
	}

	resp, err := p.testService.Add(test)

	if err != nil {
		return nil, err
	}

	return &model.PingPongResponse{
		Msg: resp,
		// Ball: input.Ball + 100,
	}, nil
}

// PingPong ...
func (p *PingPongController) Update(input model.TestInput) (*model.PingPongResponse, error) {
	datetime := time.Now()

	test := &entity.Test{
		ID:        *input.ID,
		NameEN:    *input.NameEn,
		NameTH:    *input.NameTh,
		UpdatedAt: &datetime,
	}

	_, err := p.testService.Update(test)

	if err != nil {
		return nil, err
	}

	return &model.PingPongResponse{
		Msg:  "Success",
		Ball: 200,
	}, nil
}

// PingPong ...
func (p *PingPongController) Get(input model.TestInput) (*model.PingPongResponse, error) {
	test := &entity.Test{
		ID: *input.ID,
	}

	resp, err := p.testService.Get(test)

	fmt.Println(resp.ID)
	if err != nil {
		return nil, err
	}

	return &model.PingPongResponse{
		Msg:  "Success",
		Ball: 200,
		Test: &model.Test{
			ID:       resp.ID,
			NameEn:   resp.NameEN,
			NameTh:   resp.NameTH,
			CreateAt: resp.CreatedAt.String(),
			UpdateAt: resp.UpdatedAt.String(),
		},
	}, nil
}
