package controller

import (
	"loan-system/internal/entity"
	"loan-system/internal/service"
)

// BranchController ...
type BranchController struct {
	branchService *service.BranchService
}

// NewBranchController ...
func NewBranchController(branchService *service.BranchService) *BranchController {
	return &BranchController{
		branchService: branchService,
	}
}

// Add
func (b *BranchController) Add(admin *entity.Branch) (*entity.Branch, error) {
	resp, err := b.branchService.Add(admin)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
