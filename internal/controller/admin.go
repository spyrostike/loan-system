package controller

import (
	"fmt"
	"loan-system/internal/entity"
	"loan-system/internal/graph/model"
	"loan-system/internal/service"
	"loan-system/internal/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// AdminController ...
type AdminController struct {
	adminService *service.AdminService
}

// NewAdminController ...
func NewAdminController(adminService *service.AdminService) *AdminController {
	return &AdminController{
		adminService: adminService,
	}
}

// Get
func (a *AdminController) Authen(c *gin.Context) {

	auth := &entity.Admin{}

	if err := c.ShouldBindJSON(&auth); err != nil {
		c.Status(http.StatusUnprocessableEntity)
		return
	}

	resp, err := a.adminService.Authen(auth)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"success":  "failed",
			"messages": err.Error(),
		})
		return
	}

	c.JSON(200, resp)
}

// Add
func (a *AdminController) Add(admin *entity.Admin) (*entity.Admin, error) {
	resp, err := a.adminService.Add(admin)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// Get
func (a *AdminController) Get(id string) (*model.AdminResponse, error) {
	resp, err := a.adminService.Get(&entity.Admin{
		ID: id,
	})
	if err != nil {
		return nil, err
	}

	var admin *model.AdminResponse
	utils.ConvertObj(&resp, &admin)

	fmt.Println("xxxxxxxxx MobileNo", resp.MobileNo)
	return admin, nil
}
