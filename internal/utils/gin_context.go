package utils

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"
)

// Crypto ...
type GinContext struct {
}

// NewCrypto ...
func NewGinContext() *GinContext {
	return &GinContext{}
}

// GinContextToContextMiddleware ...
func (g *GinContext) GinContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), "GinContextKey", c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

// GinContextFromContext ...
func (g *GinContext) GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value("GinContextKey")
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}
