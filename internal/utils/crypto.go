package utils

import (
	"errors"
	"fmt"
	"loan-system/internal/config"
	"loan-system/internal/model"
	"loan-system/internal/utils/convert"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

// Crypto ...
type Crypto struct {
	config *config.Configuration
}

// NewCrypto ...
func NewCrypto(config *config.Configuration) *Crypto {
	return &Crypto{
		config: config,
	}
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func (c *Crypto) CreateToken(userId string, role string) (string, error) {
	var err error
	//Creating Access Token

	claims := model.AuthTokenClaim{
		UserID: userId,
		Role:   role,
		StandardClaims: &jwt.StandardClaims{
			// ExpiresAt: time.Now().Add(c.TokenDuration).Unix(),
			IssuedAt: time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte(c.config.JWTSecretKEY))
	if err != nil {
		return "", err
	}
	return t, nil
}

func (c *Crypto) ValidateToken(encodedToken string) (*jwt.Token, error) {
	return jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		if _, isvalid := token.Method.(*jwt.SigningMethodHMAC); !isvalid {
			return nil, fmt.Errorf("invalid token %v", token.Header["alg"])
		}
		return []byte(c.config.JWTSecretKEY), nil
	})
}

func (c *Crypto) DecodeToken(encodedToken string) (*model.AuthTokenClaim, error) {
	token, err := jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		if _, isvalid := token.Method.(*jwt.SigningMethodHMAC); !isvalid {
			return "", fmt.Errorf("invalid token %v", token.Header["alg"])
		}
		return []byte(c.config.JWTSecretKEY), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("claim is invalid")
	}

	authTokenClaims := &model.AuthTokenClaim{}
	convert.ConvertStructToStruct(claims, authTokenClaims)

	return authTokenClaims, nil
}
