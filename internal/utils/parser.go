package utils

import "encoding/json"

// ConvertObj ...
func ConvertObj(source interface{}, dest interface{}) {
	jsonSource, _ := json.Marshal(source)
	json.Unmarshal(jsonSource, &dest)
}
