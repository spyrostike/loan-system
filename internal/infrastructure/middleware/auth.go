package middleware

import (
	"loan-system/internal/config"
	"loan-system/internal/utils"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

// Admin ...
type Auth struct {
	config *config.Configuration
	crypto *utils.Crypto
}

// NewAuth ...
func NewAuth(config *config.Configuration, crypto *utils.Crypto) *Auth {
	return &Auth{
		config: config,
		crypto: crypto,
	}
}

// func (a *Auth) AuthMiddleware() gin.HandlerFunc {
func (a *Auth) AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		hasBearer := strings.HasPrefix(authHeader, a.config.BEARER_SCHEMA)
		if !hasBearer {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": "Authorization invalid",
			})
			return
		}

		tokenString := authHeader[len(a.config.BEARER_SCHEMA):]
		_, err := a.crypto.ValidateToken(tokenString)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": err.Error(),
			})
			return
		}
	}
}
