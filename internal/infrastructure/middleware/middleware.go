package middleware

import "go.uber.org/fx"

var Module = fx.Provide(
	NewAuth,
	NewDirective,
	NewHeader,
)
