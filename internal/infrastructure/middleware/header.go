package middleware

import (
	"context"
	"loan-system/internal/config"
	"loan-system/internal/utils"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

// Header ...
type Header struct {
	config    *config.Configuration
	crypto    *utils.Crypto
	ginConext *utils.GinContext
}

// NewHeader ...
func NewHeader(config *config.Configuration, crypto *utils.Crypto, ginConext *utils.GinContext) *Header {
	return &Header{
		config:    config,
		crypto:    crypto,
		ginConext: ginConext,
	}
}

// Header
const (
	userID        = "user-id"
	UUID          = "UUID"
	language      = "lang"
	Authorization = "Authorization"
)

// ContextKey
const (
	userIDContextKey        = "userIDContextKey"
	uuidContextKey          = "uuidContextKey"
	languageContextKey      = "languageContextKey"
	authorizationContextKey = "authorizationContextKey"
)

func (h *Header) UserIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		userID := c.GetHeader(userID)
		if userID == "" {
			c.String(http.StatusUnauthorized, "unauthorized")
			c.Abort()
			return
		}
		c.Set(userIDContextKey, userID)
	}
}

func (h *Header) HeaderMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// c.Set(uuidContextKey, c.GetHeader(UUID))
		// c.Set(languageContextKey, c.GetHeader(language))
		const BEARER_SCHEMA = "Bearer "
		hasBearer := strings.HasPrefix(c.GetHeader(Authorization), BEARER_SCHEMA)
		if hasBearer {
			c.Set(authorizationContextKey, c.GetHeader(Authorization)[len(BEARER_SCHEMA):])
		}
	}
}

func (h *Header) ForUUIDContext(ctx context.Context) string {
	gc, err := h.ginConext.GinContextFromContext(ctx)
	if err != nil {
		return ""
	}
	ctxData, _ := gc.Get(uuidContextKey)
	d, ok := ctxData.(string)
	if !ok {
		return ""
	}
	return d
}

func (h *Header) ForUserIDContext(ctx context.Context) string {
	gc, err := h.ginConext.GinContextFromContext(ctx)
	if err != nil {
		return ""
	}
	ctxData, _ := gc.Get(userIDContextKey)
	d, ok := ctxData.(string)
	if !ok {
		return ""
	}
	return d
}

func (h *Header) ForAuthorizationContext(ctx context.Context) string {
	gc, err := h.ginConext.GinContextFromContext(ctx)
	if err != nil {
		return ""
	}

	ctxData, _ := gc.Get(authorizationContextKey)
	d, ok := ctxData.(string)
	if !ok {
		return ""
	}

	return d
}

// ForLanguageContext ...
func (h *Header) ForLanguageContext(ctx context.Context) string {
	gc, err := h.ginConext.GinContextFromContext(ctx)
	if err != nil {
		return ""
	}
	ctxData, _ := gc.Get(languageContextKey)
	d, ok := ctxData.(string)
	if !ok {
		return ""
	}
	return d
}
