package middleware

import (
	"context"
	"fmt"
	"loan-system/internal/config"
	"loan-system/internal/graph/model"
	"loan-system/internal/utils"

	"github.com/99designs/gqlgen/graphql"
)

// Directive ...
type Directive struct {
	config *config.Configuration
	crypto *utils.Crypto
	header *Header
}

// NewAuth ...
func NewDirective(config *config.Configuration, crypto *utils.Crypto, header *Header) *Directive {
	return &Directive{
		config: config,
		crypto: crypto,
		header: header,
	}
}

func (d *Directive) HasRoleDirective(ctx context.Context, obj interface{}, next graphql.Resolver, role []model.Role) (res interface{}, err error) {
	encodedToken := d.header.ForAuthorizationContext(ctx)
	authClaimsToken, err := d.crypto.DecodeToken(encodedToken)

	if err != nil {
		return nil, err
	}

	for _, s := range role {
		fmt.Println(authClaimsToken.Role, string(s))
		if authClaimsToken.Role == string(s) {
			return next(ctx)
		}
	}

	return nil, fmt.Errorf("access denied")
}
