package http

import (
	"context"
	"loan-system/internal/config"
	"loan-system/internal/gin/route"
	"loan-system/internal/graph/generated"
	"loan-system/internal/graph/resolver"
	"loan-system/internal/infrastructure/middleware"
	"loan-system/internal/utils"
	"log"
	"net/http"

	"go.uber.org/fx"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
)

var Module = fx.Provide(NewHTTPServer)

// GinHTTPServer ..
type GinHTTPServer struct {
	auth      *middleware.Auth
	config    *config.Configuration
	directive *middleware.Directive
	header    *middleware.Header
	ginConext *utils.GinContext
	route     *gin.Engine
	router    router
	resolver  *resolver.Resolver
	server    *http.Server
}

type router struct {
	fx.In
	AdminRoute *route.AdminRoute
}

// NewHTTPServer ..
func NewHTTPServer(
	auth *middleware.Auth,
	config *config.Configuration,
	crypto *utils.Crypto,
	directive *middleware.Directive,
	header *middleware.Header,
	ginConext *utils.GinContext,
	resolver *resolver.Resolver,
	router router,
) *GinHTTPServer {
	r := gin.New()
	r.Use(ginConext.GinContextToContextMiddleware(), header.HeaderMiddleware())

	r.Use(gin.Recovery())

	return &GinHTTPServer{
		auth:      auth,
		config:    config,
		directive: directive,
		ginConext: ginConext,
		header:    header,
		route:     r,
		resolver:  resolver,
		router:    router,
	}
}

// Configure ...
func (g *GinHTTPServer) configure() *gin.Engine {
	r := g.route
	// Health check
	r.GET("/healthz", func(c *gin.Context) {
		c.String(200, "OK")
	})

	r.GET("/ping", g.auth.AuthMiddleware(), func(c *gin.Context) {
		c.JSON(200, gin.H{
			"ping": "pong",
		})
	})

	g.router.AdminRoute.RoutesGrouping(r)

	r.GET("/", g.auth.AuthMiddleware(), func(c *gin.Context) {
		srv := playground.Handler("GraphQL playground", "/query")
		srv.ServeHTTP(c.Writer, c.Request)
	})

	r.POST("/query", g.auth.AuthMiddleware(), func(c *gin.Context) {
		config := generated.Config{Resolvers: g.resolver}
		config.Directives.HasRole = g.directive.HasRoleDirective
		srv := handler.NewDefaultServer(generated.NewExecutableSchema(config))
		srv.ServeHTTP(c.Writer, c.Request)
	})

	return r
}

func (g *GinHTTPServer) Start(_ context.Context) {
	router := g.configure()
	g.server = &http.Server{
		Addr:    ":" + g.config.Port,
		Handler: router,
	}

	go func() {
		if err := g.server.ListenAndServe(); err != nil {
			log.Fatalf("listen: %s\n\n", err)
		}
	}()

	log.Println("Listening and serving HTTP on", g.config.Port)
}

func (g *GinHTTPServer) Stop(ctx context.Context) error {
	return g.server.Shutdown(ctx)
}
