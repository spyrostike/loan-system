package database

import (
	"database/sql"
	"fmt"
	"loan-system/internal/config"
	"loan-system/internal/entity"
	"log"
	"time"

	"go.uber.org/fx"
	"gorm.io/gorm/logger"

	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Module = fx.Provide(NewDB)

type DB struct {
	Connection   *gorm.DB
	MasterImages *gorm.DB
	sql          *sql.DB
	config       *config.Configuration
}

// NewDB is start connection database.
func NewDB(config *config.Configuration) (*DB, error) {
	database := &DB{
		config: config,
	}

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s", config.DBHost, config.DBUser, config.DBPassword, config.DBName, config.DBPort, config.SSLMode, config.TimeZone)
	db, sqlDB, err := database.connectPostgresDatabase(dsn)

	if err != nil {
		return nil, err
	}

	database.Connection = db
	database.sql = sqlDB

	return database, nil
}

func (db *DB) connectPostgresDatabase(dsn string) (*gorm.DB, *sql.DB, error) {
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})

	if err != nil {
		return nil, nil, err
	}

	sqlDB, err := database.DB()

	if err != nil {
		return nil, nil, err
	}

	if db.config.Environment != "production" {
		database.Logger = logger.Default.LogMode(logger.Info)
	}

	sqlDB.SetConnMaxLifetime(time.Minute * 5)
	sqlDB.SetMaxOpenConns(7)
	sqlDB.SetMaxIdleConns(5)

	return database, sqlDB, nil
}

// Close Connection DB
func (db *DB) Close() {
	if err := db.sql.Close(); err != nil {
		log.Printf("Error closing db connection %s", err)
		return
	}
	log.Println("DB connection closed")
}

func (db *DB) Migrate() {
	println("Migrating Database...")

	var test entity.Test
	var geography entity.Geography
	var province entity.Province
	var district entity.District
	var subDistrict entity.SubDistrict

	var branch entity.Branch
	var department entity.Department
	var position entity.Position
	var imageProfile entity.ImageProfile
	var admin entity.Admin

	loanSystem := gormigrate.New(db.Connection, gormigrate.DefaultOptions, []*gormigrate.Migration{
		// Clean DB
		{
			// ID format is time format yyyyMMddHHmmss - ß20060102150405
			ID:       "00000000000000",
			Migrate:  func(tx *gorm.DB) error { return nil },
			Rollback: func(tx *gorm.DB) error { return nil },
		},
		// First migration
		{
			ID: "20210517212350",
			Migrate: func(tx *gorm.DB) error {
				/*********************************
				* Master Data
				*********************************/
				type Test struct {
					ID        string     `gorm:"type:uuid;primary_key;" json:"id"`
					NameEN    string     `gorm:"type:varchar(50)" json:"nameEN"`
					NameTH    string     `gorm:"type:varchar(50)" json:"nameTH"`
					CreatedAt *time.Time `gorm:"column:created_at" json:"createdAt"`
					UpdatedAt *time.Time `gorm:"column:updated_at" json:"updatedAt"`
					DeletedAt *time.Time `gorm:"column:deleted_at" json:"deletedAt"`
				}

				type Geography struct {
					ID   int    `gorm:"column:id;primary_key;" json:"id"`
					Name string `gorm:"type:varchar(255)" json:"name"`
				}

				type Province struct {
					ID          int    `gorm:"column:id;primary_key;" json:"id"`
					Code        string `gorm:"type:varchar(2)" json:"code"`
					NameEN      string `gorm:"type:varchar(150)" json:"nameEN"`
					NameTH      string `gorm:"type:varchar(150)" json:"nameTH"`
					GeographyID int    `json:"geographyId"`
				}

				type District struct {
					ID         int    `gorm:"column:id;primary_key;" json:"id"`
					Code       string `gorm:"type:varchar(4)" json:"code"`
					NameEN     string `gorm:"type:varchar(150)" json:"nameEN"`
					NameTH     string `gorm:"type:varchar(150)" json:"NameTH"`
					ProvinceID int    `json:"provinceID"`
				}

				type SubDistrict struct {
					ID         int    `gorm:"column:id;primary_key;" json:"id"`
					ZipCode    int    `json:"zipCode"`
					NameEN     string `gorm:"type:varchar(150)" json:"nameEN"`
					NameTH     string `gorm:"type:varchar(150)" json:"NameTH"`
					DistrictID int    `json:"district_id"`
				}

				type Branch struct {
					ID        string        `gorm:"type:uuid;primary_key;" json:"id"`
					Code      string        `gorm:"type:varchar(10)" json:"code"`
					NameEN    string        `gorm:"type:varchar(150)" json:"nameEn"`
					NameTH    string        `gorm:"type:varchar(150)" json:"nameTh"`
					Status    entity.Status `sql:"type:enum('ACTIVE', 'INACTIVE')" json:"status"`
					CreatedAt *time.Time    `gorm:"column:created_at" json:"createdAt"`
					CreatedBy *string       `gorm:"type:uuid" json:"createdBy"`
					UpdatedAt *time.Time    `gorm:"column:updated_at" json:"updatedAt"`
					UpdatedBy *string       `gorm:"type:uuid" json:"updatedBy"`
					DeletedAt *time.Time    `gorm:"column:deleted_at" json:"deletedAt"`
					DeletedBy *string       `gorm:"type:uuid" json:"deletedBy"`
				}

				type Department struct {
					ID        string        `gorm:"type:uuid;primary_key;" json:"id"`
					Code      string        `gorm:"type:varchar(10)" json:"code"`
					NameEN    string        `gorm:"type:varchar(150)" json:"nameEN"`
					NameTH    string        `gorm:"type:varchar(150)" json:"nameTH"`
					Status    entity.Status `sql:"type:enum('ACTIVE', 'INACTIVE')" json:"status"`
					CreatedAt *time.Time    `gorm:"column:created_at" json:"createdAt"`
					CreatedBy *string       `gorm:"type:uuid" json:"createdBy"`
					UpdatedAt *time.Time    `gorm:"column:updated_at" json:"updatedAt"`
					UpdatedBy *string       `gorm:"type:uuid" json:"updatedBy"`
					DeletedAt *time.Time    `gorm:"column:deleted_at" json:"deletedAt"`
					DeletedBy *string       `gorm:"type:uuid" json:"deletedBy"`
				}

				type Position struct {
					ID        string        `gorm:"type:uuid;primary_key;" json:"id"`
					Code      string        `gorm:"type:varchar(10)" json:"code"`
					NameEN    string        `gorm:"type:varchar(150)" json:"nameEN"`
					NameTH    string        `gorm:"type:varchar(150)" json:"nameTH"`
					Status    entity.Status `sql:"type:enum('ACTIVE', 'INACTIVE')" json:"status"`
					CreatedAt *time.Time    `gorm:"column:created_at" json:"createdAt"`
					CreatedBy *string       `gorm:"type:uuid" json:"createdBy"`
					UpdatedAt *time.Time    `gorm:"column:updated_at" json:"updatedAt"`
					UpdatedBy *string       `gorm:"type:uuid" json:"updatedBy"`
					DeletedAt *time.Time    `gorm:"column:deleted_at" json:"deletedAt"`
					DeletedBy *string       `gorm:"type:uuid" json:"deletedBy"`
				}

				type ImageProfile struct {
					ID        string        `gorm:"type:uuid;primary_key;" json:"id"`
					Path      string        `gorm:"type:varchar(20);" json:"path"`
					Name      string        `gorm:"type:varchar(30);" json:"name"`
					Type      string        `gorm:"type:varchar(20);" json:"type"`
					SubType   string        `gorm:"type:varchar(20);" json:"subType"`
					Size      int           `json:"size"`
					Tag       string        `gorm:"type:varchar(20);" json:"tag"`
					RefID     string        `gorm:"type:uuid;" json:"refID"`
					Status    entity.Status `sql:"type:enum('ACTIVE', 'INACTIVE');" json:"status"`
					CreatedAt *time.Time    `gorm:"column:created_at" json:"createdAt"`
					CreatedBy *string       `gorm:"type:uuid" json:"createdBy"`
					UpdatedAt *time.Time    `gorm:"column:updated_at" json:"updatedAt"`
					UpdatedBy *string       `gorm:"type:uuid" json:"updatedBy"`
					DeletedAt *time.Time    `gorm:"column:deleted_at" json:"deletedAt"`
					DeletedBy *string       `gorm:"type:uuid" json:"deletedBy"`
				}

				type Admin struct {
					ID            string           `gorm:"type:uuid;primary_key;" json:"id"`
					Username      string           `gorm:"type:varchar(100);unique_index;not null;" json:"username"`
					Password      string           `gorm:"type:varchar(120);not null;" json:"password"`
					FirstName     string           `gorm:"type:varchar(100);not null;" json:"firstName"`
					LastName      string           `gorm:"type:varchar(100);not null;" json:"lastName"`
					Email         string           `gorm:"type:varchar(50);not null;" json:"email"`
					MobileNo      string           `gorm:"type:varchar(10);" json:"mobileNo"`
					ImageProfiles []ImageProfile   `gorm:"foreignKey:RefID;references:ID"`
					Role          entity.AdminRole `sql:"type:enum('SUPER_ADMIN', 'GENERAL_ADMIN');" json:"role"`
					Status        entity.Status    `sql:"type:enum('ACTIVE', 'INACTIVE');" json:"status"`
					CreatedAt     *time.Time       `gorm:"column:created_at" json:"createdAt"`
					CreatedBy     *string          `gorm:"type:uuid" json:"createdBy"`
					UpdatedAt     *time.Time       `gorm:"column:updated_at" json:"updatedAt"`
					UpdatedBy     *string          `gorm:"type:uuid" json:"updatedBy"`
					DeletedAt     *time.Time       `gorm:"column:deleted_at" json:"deletedAt"`
					DeletedBy     *string          `gorm:"type:uuid" json:"deletedBy"`
				}

				if err := tx.Table(test.TableName()).AutoMigrate(&Test{}); err != nil {
					return err
				}

				if err := tx.Table(geography.TableName()).AutoMigrate(&Geography{}); err != nil {
					return err
				}

				if err := tx.Table(province.TableName()).AutoMigrate(&Province{}); err != nil {
					return err
				}

				if err := tx.Table(district.TableName()).AutoMigrate(&District{}); err != nil {
					return err
				}

				if err := tx.Table(subDistrict.TableName()).AutoMigrate(&SubDistrict{}); err != nil {
					return err
				}

				if err := tx.Table(branch.TableName()).AutoMigrate(&Branch{}); err != nil {
					return err
				}

				if err := tx.Table(department.TableName()).AutoMigrate(&Department{}); err != nil {
					return err
				}

				if err := tx.Table(position.TableName()).AutoMigrate(&Position{}); err != nil {
					return err
				}

				if err := tx.Table(imageProfile.TableName()).AutoMigrate(&ImageProfile{}); err != nil {
					return err
				}

				if err := tx.Table(admin.TableName()).AutoMigrate(&Admin{}); err != nil {
					return err
				}

				tx.Exec("DROP TYPE IF EXISTS STATUS;CREATE TYPE STATUS AS ENUM ('ACTIVE', 'INACTIVE')")
				tx.Exec("DROP TYPE IF EXISTS ADMIN_ROLE;CREATE TYPE ADMIN_ROLE AS ENUM ('SUPER_ADMIN', 'GENERAL_ADMIN')")

				tx.Exec("ALTER TABLE branches ALTER COLUMN status TYPE status using status::status;")
				tx.Exec("ALTER TABLE departments ALTER COLUMN status TYPE status using status::status;")
				tx.Exec("ALTER TABLE positions ALTER COLUMN status TYPE status using status::status;")
				tx.Exec("ALTER TABLE admins ALTER COLUMN status TYPE status using status::status;")
				tx.Exec("ALTER TABLE admins ALTER COLUMN role TYPE admin_role using role::admin_role;")

				return nil
			},
			Rollback: func(tx *gorm.DB) error {
				if err := tx.Migrator().DropTable(test.TableName()); err != nil {
					return err
				}

				return nil
			},
		},
	})

	if err := loanSystem.Migrate(); err != nil {
		log.Panicf("Could not migrate loan_system: %v", err.Error())
	}

	// if err := migration.RollbackTo("00000000000000"); err != nil {
	// 	log.Panicf("Could not rollback database: %v", err.Error())
	// }

	log.Println("Migration did run successfully")
}
