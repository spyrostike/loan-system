package repository

import (
	"loan-system/internal/infrastructure/database"
)

type CRUDRepository struct {
	Database *database.DB
}

func NewCRUDRepository(
	db *database.DB,
) *CRUDRepository {
	return &CRUDRepository{
		Database: db,
	}
}

func (r *CRUDRepository) Create(input interface{}) error {
	tx := r.Database.Connection.Begin()

	err := tx.Create(input).Error

	if err != nil {
		tx.Rollback()

		return err
	}

	tx.Commit()

	return nil
}

func (r *CRUDRepository) GetOne(where interface{}, target interface{}) error {
	err := r.Database.Connection.First(target, where).Error

	if err != nil {
		return err
	}
	return nil
}

func (r *CRUDRepository) Get(where interface{}, target interface{}) error {
	err := r.Database.Connection.Find(target, where).Error

	if err != nil {
		return err
	}
	return nil
}

func (r *CRUDRepository) Count(model interface{}, where interface{}) (int64, error) {
	var count int64
	err := r.Database.Connection.Model(model).Where(where).Count(&count).Error

	if err != nil {
		return 0, nil
	}

	return count, nil
}

func (r *CRUDRepository) Delete(target interface{}, where interface{}) error {
	return r.Database.Connection.Delete(target, where).Error
}

func (r *CRUDRepository) Update(target interface{}, where interface{}) error {
	err := r.Database.Connection.Model(target).Where(where).Updates(target).Error

	if err != nil {
		return err
	}

	return nil
}
