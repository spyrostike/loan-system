package repository

import (
	"go.uber.org/fx"
)

var Module = fx.Provide(
	NewAdminRepository,
	NewBranchRepository,
	NewCRUDRepository,
	NewTestRepository,
)
