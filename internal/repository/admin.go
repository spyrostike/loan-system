package repository

import (
	"loan-system/internal/entity"
	"loan-system/internal/infrastructure/database"
)

type Admin struct {
	database *database.DB
	crud     *CRUDRepository
}

func NewAdminRepository(database *database.DB, crud *CRUDRepository) *Admin {
	return &Admin{
		database: database,
		crud:     crud,
	}
}

// Authen...
func (repo *Admin) Authen(admin *entity.Admin) (*entity.Admin, error) {
	model := entity.Admin{
		Username: admin.Username,
	}
	res := &entity.Admin{}
	err := repo.database.Connection.First(res, model).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}

// GetByCondition...
func (repo *Admin) GetByCondition(admin *entity.Admin) (*entity.Admin, error) {
	res := &entity.Admin{}
	err := repo.database.Connection.First(res, admin).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}

// Add admin
func (repo *Admin) Add(admin *entity.Admin) (*entity.Admin, error) {
	err := repo.database.Connection.Create(admin).Error
	if err != nil {
		return nil, err
	}
	return admin, nil
}

// Get admin by Id
func (repo *Admin) Get(admin *entity.Admin) (*entity.Admin, error) {
	model := entity.Admin{
		ID: admin.ID,
	}
	res := &entity.Admin{}
	err := repo.database.Connection.Preload("ImageProfiles").First(res, model).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}
