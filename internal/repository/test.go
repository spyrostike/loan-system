package repository

import (
	"loan-system/internal/entity"
	"loan-system/internal/infrastructure/database"
)

type Test struct {
	database *database.DB
	crud     *CRUDRepository
}

func NewTestRepository(database *database.DB, crud *CRUDRepository) *Test {
	return &Test{
		database: database,
		crud:     crud,
	}
}

// CreateUpdateCustomer is used to update or create
func (repo *Test) Add(test *entity.Test) error {
	err := repo.crud.Create(&test)
	if err != nil {
		return err
	}
	return nil
}

// CreateUpdateCustomer is used to update or create
func (repo *Test) Update(test *entity.Test) error {
	model := entity.Test{
		ID: test.ID,
	}
	err := repo.crud.Update(&test, &model)
	if err != nil {
		return err
	}
	return nil
}

// CreateUpdateCustomer is used to update or create
func (repo *Test) Get(test *entity.Test, target interface{}) error {
	model := entity.Test{
		ID: test.ID,
	}
	err := repo.crud.GetOne(model, target)
	if err != nil {
		return err
	}
	return nil
}
