package repository

import (
	"loan-system/internal/entity"
	"loan-system/internal/infrastructure/database"
)

type Branch struct {
	database *database.DB
	crud     *CRUDRepository
}

func NewBranchRepository(database *database.DB, crud *CRUDRepository) *Branch {
	return &Branch{
		database: database,
		crud:     crud,
	}
}

// Add
func (repo *Branch) Add(branch *entity.Branch) (*entity.Branch, error) {
	err := repo.database.Connection.Create(branch).Error
	if err != nil {
		return nil, err
	}
	return branch, nil
}

// Get
func (repo *Branch) Get(branch *entity.Branch) (*entity.Branch, error) {
	res := &entity.Branch{}
	err := repo.database.Connection.First(res, branch).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}
