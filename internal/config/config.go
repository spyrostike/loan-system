package config

import (
	"time"

	"github.com/caarlos0/env/v6"
	"go.uber.org/fx"
)

var Module = fx.Provide(NewConfiguration)

// Configuration ...
type Configuration struct {
	// this section is set env param
	Port        string `env:"PORT" envDefault:"3000"`
	Environment string `env:"ENV" envDefault:"localhost"`

	DBHost     string `env:"DB_HOST" envDefault:"127.0.0.1"`
	DBPort     string `env:"DB_PORT" envDefault:"5432"`
	DBUser     string `env:"DB_USER" envDefault:"loan_system"`
	DBName     string `env:"DB_NAME" envDefault:"loan_system"`
	DBPassword string `env:"DB_PASSWORD" envDefault:"p@ssword"`
	SSLMode    string `env:"SSL_MODE" envDefault:"disable"`
	TimeZone   string `env:"TIME_ZONE" envDefault:"Asia/bangkok"`

	JWTSecretKEY  string        `env:"JWT_SECRET_KEY" envDefault:"ads213edfouhiyghi3j249y18230129cv2"`
	BEARER_SCHEMA string        `env:"BEARER_SCHEMA" envDefault:"Bearer "`
	TokenDuration time.Duration `env:"TOKEN_DURATION" envDefault:"30m"`
}

// NewConfiguration ...
func NewConfiguration() (*Configuration, error) {
	cfg := new(Configuration)
	if err := env.Parse(cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
