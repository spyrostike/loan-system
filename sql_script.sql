DROP TYPE IF EXISTS STATUS;
CREATE TYPE STATUS AS ENUM ('ACTIVE', 'INACTIVE');
DROP TYPE IF EXISTS ADMIN_ROLE;
CREATE TYPE ADMIN_ROLE AS ENUM ('SUPER_ADMIN', 'ADMIN');

DROP TYPE IF EXISTS USER_ROLE;
CREATE TYPE USER_ROLE AS ENUM ('USER');

CREATE TABLE IF NOT EXISTS test (
  id uuid PRIMARY KEY,
  name_en VARCHAR(50) NOT NULL,
  name_th VARCHAR(50) NOT NULL,
  created_at timestamptz(6) NOT NULL,
  updated_at timestamptz(6) NOT NULL,
  deleted_at timestamptz(6)
);

CREATE TABLE IF NOT EXISTS geographies (
  id INT PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS provinces (
  id INT PRIMARY KEY,
  code VARCHAR(2) NOT NULL,
  name_en VARCHAR(150) NOT NULL,
  name_th VARCHAR(150) NOT NULL,
  geography_id INT NOT NULL
);

CREATE TABLE IF NOT EXISTS districts (
  id INT PRIMARY KEY,
  code VARCHAR(4) NOT NULL,
  name_en VARCHAR(150) NOT NULL,
  name_th VARCHAR(150) NOT NULL,
  province_id INT NOT NULL
);

CREATE TABLE IF NOT EXISTS sub_districts (
  id INT PRIMARY KEY,
  zip_code INT NOT NULL,
  name_en VARCHAR(150) NOT NULL,
  name_th VARCHAR(150) NOT NULL,
  district_id INT NOT NULL
);

CREATE TABLE IF NOT EXISTS branches (
  id uuid PRIMARY KEY,
  code VARCHAR(10) NOT NULL,
  name_en VARCHAR(150) NOT NULL,
  name_th VARCHAR(150) NOT NULL,
  status status DEFAULT NULL,
  created_at timestamptz(6),
  created_by uuid,
  updated_at timestamptz(6),
  updated_by uuid,
  deleted_at timestamptz(6),
  deleted_by uuid
);

CREATE TABLE IF NOT EXISTS departments (
  id uuid PRIMARY KEY,
  code VARCHAR(10) NOT NULL,
  name_en VARCHAR(150) NOT NULL,
  name_th VARCHAR(150) NOT NULL,
  status status DEFAULT NULL,
  created_at timestamptz(6),
  created_by uuid,
  updated_at timestamptz(6),
  updated_by uuid,
  deleted_at timestamptz(6),
  deleted_by uuid
);

CREATE TABLE IF NOT EXISTS positions (
  id uuid PRIMARY KEY,
  code VARCHAR(10) NOT NULL,
  name_en VARCHAR(150) NOT NULL,
  name_th VARCHAR(150) NOT NULL,
  status status DEFAULT NULL,
  created_at timestamptz(6),
  created_by uuid,
  updated_at timestamptz(6),
  updated_by uuid,
  deleted_at timestamptz(6),
  deleted_by uuid
);

CREATE TABLE IF NOT EXISTS image_profiles (
  id uuid PRIMARY KEY,
  path VARCHAR(20) NOT NULL,
  name VARCHAR(30) NOT NULL,
  type VARCHAR(20) NOT NULL,
  sub_type VARCHAR(20) NOT NULL,
  size INT NOT NULL,
  tag VARCHAR(40) NOT NULL,
  ref_id uuid,
  status status DEFAULT NULL,
  created_at timestamptz(6),
  created_by uuid,
  updated_at timestamptz(6),
  updated_by uuid,
  deleted_at timestamptz(6),
  deleted_by uuid
);

CREATE TABLE IF NOT EXISTS admins (
  id uuid PRIMARY KEY,
  username VARCHAR(100) UNIQUE NOT NULL,
  password VARCHAR(120) NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  email VARCHAR(50) UNIQUE NOT NULL,
  mobile_no VARCHAR(10) NOT NULL,
  role ADMIN_ROLE DEFAULT NULL,
  status status DEFAULT NULL,
  created_at timestamptz(6),
  created_by uuid,
  updated_at timestamptz(6),
  updated_by uuid,
  deleted_at timestamptz(6),
  deleted_by uuid
);

CREATE TABLE IF NOT EXISTS users (
  id uuid PRIMARY KEY,
  username VARCHAR(100) NOT NULL,
  password VARCHAR(120) NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  email VARCHAR(50) NOT NULL,
  mobile_no VARCHAR(10) NOT NULL,
  salary_rate decimal(14, 2),
  position_id uuid,
  department_id uuid,
  branch_id uuid,
  gender status DEFAULT NULL,
  address text,
  province_id INT,
  district_id INT,
  sub_district_id INT,
  role ADMIN_ROLE DEFAULT NULL,
  status status DEFAULT NULL,
  created_at timestamptz(6),
  created_by uuid,
  updated_at timestamptz(6),
  updated_by uuid,
  deleted_at timestamptz(6),
  deleted_by uuid
);

ALTER TABLE IF EXISTS users DROP CONSTRAINT IF EXISTS users_position_id;
ALTER TABLE IF EXISTS users DROP CONSTRAINT IF EXISTS users_department_id;
ALTER TABLE IF EXISTS users DROP CONSTRAINT IF EXISTS users_branch_id;
ALTER TABLE IF EXISTS users DROP CONSTRAINT IF EXISTS users_province_id;
ALTER TABLE IF EXISTS users DROP CONSTRAINT IF EXISTS users_district_id;
ALTER TABLE IF EXISTS users DROP CONSTRAINT IF EXISTS users_sub_district_id;

ALTER TABLE users ADD CONSTRAINT users_position_id FOREIGN KEY (position_id) REFERENCES positions (id);
ALTER TABLE users ADD CONSTRAINT users_department_id FOREIGN KEY (department_id) REFERENCES departments (id);
ALTER TABLE users ADD CONSTRAINT users_branch_id FOREIGN KEY (branch_id) REFERENCES branches (id);
ALTER TABLE users ADD CONSTRAINT users_province_id FOREIGN KEY (province_id) REFERENCES provinces (id);
ALTER TABLE users ADD CONSTRAINT users_district_id FOREIGN KEY (district_id) REFERENCES districts (id);
ALTER TABLE users ADD CONSTRAINT users_sub_district_id FOREIGN KEY (sub_district_id) REFERENCES sub_districts (id);





-- ALTER TABLE IF EXISTS image_profiles DROP CONSTRAINT IF EXISTS image_profiles_ref_id;
-- ALTER TABLE image_profiles ADD CONSTRAINT image_profiles_ref_id FOREIGN KEY (ref_id) REFERENCES admins (id)
